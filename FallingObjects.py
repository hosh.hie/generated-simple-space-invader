import pygame
import random

# Initialisierung von Pygame
pygame.init()

# Bildschirmgröße
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))

# Variable für die Geschwindigkeit der herabfallenden Objekte
falling_object_speed = 1

# Farben
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

# Zeit-Tracking für die Erhöhung der Geschwindigkeit
speed_increase_interval = 5000  # Millisekunden
last_speed_increase = pygame.time.get_ticks()

# Variable für die Punktzahl des Spielers
player_score = 0


def show_text(text, x, y, size=36, color=BLACK):
    font = pygame.font.Font(None, size)
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect()
    text_rect.centerx = x
    text_rect.centery = y
    screen.blit(text_surface, text_rect)

# Spielerklasse
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((50, 50))
        self.image.fill(BLACK)
        self.rect = self.image.get_rect(center=(screen_width // 2, screen_height - 50))
        self.speed = 5

    def update(self, action):
        if action == 'LEFT' and self.rect.left > 0:
            self.rect.x -= self.speed
        if action == 'RIGHT' and self.rect.right < screen_width:
            self.rect.x += self.speed

# Fallendes Objekt
class FallingObject(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((50, 50))
        self.image.fill(RED)
        self.rect = self.image.get_rect(center=(random.randint(0, screen_width), 0))

    def update(self):
        self.rect.y += 5
        #if self.rect.top > screen_height:
        #    self.kill()

# Spiel-Setup
player = Player()
falling_objects = pygame.sprite.Group()

# Spiel-Schleife
running = True
clock = pygame.time.Clock()
while running:
    # Aktuelle Zeit
    current_time = pygame.time.get_ticks()

    # Überprüfen, ob es Zeit ist, die Geschwindigkeit zu erhöhen
    if current_time - last_speed_increase > speed_increase_interval:
        falling_object_speed += 1  # Geschwindigkeit erhöhen
        last_speed_increase = current_time

    # Ereignisse verarbeiten
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Spieleraktionen
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player.update('LEFT')
    if keys[pygame.K_RIGHT]:
        player.update('RIGHT')

    # Neue fallende Objekte hinzufügen
    if random.randint(1, 20) == 1:
        new_object = FallingObject()
        falling_objects.add(new_object)

    # Update der herabfallenden Objekte mit der neuen Geschwindigkeit
    for obj in falling_objects:
        obj.rect.y += falling_object_speed
        
        if obj.rect.top > screen_height:
            obj.kill()
            player_score += 100  # Punktzahl erhöhen, wenn ein Alien den unteren Bildschirmrand erreicht

    # Spiellogik
    falling_objects.update()

    # Kollisionserkennung
    if pygame.sprite.spritecollideany(player, falling_objects):
        running = False

    # Bildschirm aktualisieren
    screen.fill(WHITE)
    falling_objects.draw(screen)
    screen.blit(player.image, player.rect)
    
    # Zeige die Punktzahl im oberen Bereich des Bildschirms an
    show_text(f"Score: {player_score}", screen_width // 2, 20)

    
    pygame.display.flip()

    # FPS
    clock.tick(60)

pygame.quit()