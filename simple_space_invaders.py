import pygame
import random
import os

# Initialize Pygame
pygame.init()

# Screen dimensions
WIDTH, HEIGHT = 800, 600
screen = pygame.display.set_mode((WIDTH, HEIGHT))

# Load images
# Load and scale images
player_img = pygame.image.load("player.png").convert_alpha()
#player_img.set_colorkey((255, 255, 255))

player_img = pygame.transform.scale(player_img, (50, 50))

invader_img = pygame.image.load("invader.png").convert()
invader_img.set_colorkey((255, 255, 255))

invader_img = pygame.transform.scale(invader_img, (40, 30))

bullet_img = pygame.image.load("bullet.png")
bullet_img = pygame.transform.scale(bullet_img, (10, 20))

bomb_img = pygame.image.load("bomb.png")
bomb_img = pygame.transform.scale(bomb_img, (15, 30))

# Fonts
font = pygame.font.Font(None, 36)

class Player(pygame.sprite.Sprite):
    def __init__(self, size=50):
        super().__init__()
        self.image = player_img
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH // 2, HEIGHT - size + 15)
        self.colRect = pygame.rect.Rect((0, 0), (size - 10, size - 10))
        self.colRect.center = self.rect.center
        

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.rect.x -= 5
        if keys[pygame.K_RIGHT]:
            self.rect.x += 5

        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > WIDTH:
            self.rect.right = WIDTH

        self.colRect.center = self.rect.center

class Invader(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = invader_img
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        global level
        self.speed = 2 * level

    def update(self):
        self.rect.x += self.speed
        # self.rect.left + self.speed < 0 or self.rect.right + self.speed > screen_width:
        if self.rect.left + self.speed < 0 or self.rect.right + self.speed > WIDTH:
            self.rect.y += 50
            self.speed = -self.speed

        if self.rect.top > HEIGHT:
            self.kill()
            running = False

        # Randomly drop bombs
        if random.random() < 0.001:
            bomb = Bomb(self.rect.centerx, self.rect.bottom)
            all_sprites.add(bomb)
            bombs.add(bomb)

class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = bullet_img
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)

    def update(self):
        self.rect.y -= 5
        if self.rect.bottom < 0:
            self.kill()

class Bomb(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = bomb_img
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)

    def update(self):
        self.rect.y += 5
        if self.rect.top > HEIGHT:
            self.kill()

def start_screen():
    screen.fill((0, 0, 0))
    text = font.render("Press Spacebar to Start", True, (255, 255, 255))
    screen.blit(text, (WIDTH // 2 - text.get_width() // 2, HEIGHT // 2 - text.get_height() // 2))
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return True

def game_over_screen():
    screen.fill((0, 0, 0))
    text = font.render("Game Over - Press Spacebar to Retry", True, (255, 255, 255))
    screen.blit(text, (WIDTH // 2 - text.get_width() // 2, HEIGHT // 2 - text.get_height() // 2))
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return True

# Add a function to display the level and score
def display_level_and_score(level, score):
    #global level, score
    level_text = font.render(f"Level: {level}", True, (255, 255, 255))
    score_text = font.render(f"Score: {score}", True, (255, 255, 255))
    screen.blit(level_text, (10, 10))
    screen.blit(score_text, (WIDTH - score_text.get_width() - 10, 10))

def main():
    # Initialize level and score
    global level 
    level = 1
    score = 0
    player = Player()
    global all_sprites
    all_sprites = pygame.sprite.Group(player)
    invaders = pygame.sprite.Group()
    bullets = pygame.sprite.Group()
    global bombs
    bombs = pygame.sprite.Group()

    for i in range(5):
        for j in range(10):
            invader = Invader(50 + j * 70, 50 + i * 50)
            all_sprites.add(invader)
            invaders.add(invader)

    global running
    running = True
    clock = pygame.time.Clock()

    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    bullet = Bullet(player.rect.centerx, player.rect.top)
                    all_sprites.add(bullet)
                    bullets.add(bullet)

        all_sprites.update()

        hits = pygame.sprite.groupcollide(invaders, bullets, True, True)
        for hit in hits:
            score += 10

        if pygame.sprite.spritecollide(player, invaders, True):
            running = False

        for bomb in bombs:
            if player.colRect.colliderect(bomb):
                running = False

        #if pygame.sprite.spritecollide(player, invaders, False):
        #    running = False

        if len(invaders) == 0:
            level += 1
            for i in range(5):
                for j in range(10):
                    invader = Invader(50 + j * 70, 50 + i * 50)
                    all_sprites.add(invader)
                    invaders.add(invader)

        screen.fill((0, 0, 0))
        all_sprites.draw(screen)
        display_level_and_score(level, score)
        pygame.display.flip()

if __name__ == "__main__":
    while True:
        if not start_screen():
            break
        main()
        if not game_over_screen():
            break

pygame.quit()