# Simple Space Invaders

This has been generated with GPT-4-32k with 3 prompts and 10 minutes of refinement :-). Even this readme is generated.

Simple Space Invaders is a simplified version of the classic Space Invaders game, created using Python and the Pygame library. The game features a player-controlled spaceship that can move left and right to shoot down incoming alien invaders while avoiding their bombs.

## Features

Player-controlled spaceship with left and right movement
Alien invaders with random movement and bomb dropping
Collision detection for player, aliens, and projectiles
Increasing difficulty with each completed level

## Installation

Ensure you have Python 3.x installed on your system. You can download it from the official Python website.

Install the Pygame library using pip:
```
pip install pygame
```
Clone the repository or download the source code:
```
git clone https://github.com/yourusername/simpleSpaceInvaders.git
```
Navigate to the project directory:
```
cd simpleSpaceInvaders
```
## Usage

Run the game using the following command:
```
python SimpleSpaceInvaders.py
```
## Controls

Left Arrow: Move spaceship left
Right Arrow: Move spaceship right
Spacebar: Shoot bullet

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

MIT