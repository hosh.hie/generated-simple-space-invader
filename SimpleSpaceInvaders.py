import pygame
import random

# Initialize Pygame
pygame.init()

# Screen size and title
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Simplified Space Invaders")

# Colors
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLACK = (0, 0, 0)

# Player class
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((40, 20))
        self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.rect.centerx = screen_width // 2
        self.rect.bottom = screen_height - 10
        self.speed = 5

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.rect.x -= self.speed
        if keys[pygame.K_RIGHT]:
            self.rect.x += self.speed

        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > screen_width:
            self.rect.right = screen_width

# Alien class
class Alien(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((40, 20))
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(screen_width - self.rect.width)
        self.rect.y = random.randrange(50, 200)
        self.speed = 1

    def update(self):
        self.rect.x += self.speed
        if self.rect.left + self.speed < 0 or self.rect.right + self.speed > screen_width:
            self.speed = -self.speed
            self.rect.y += 20

        # Randomly drop bombs
        if random.random() < 0.01:
            bomb = Bomb(self.rect.centerx, self.rect.bottom)
            all_sprites.add(bomb)
            bombs.add(bomb)

# Bullet class
class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((5, 10))
        self.image.fill(BLACK)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.top = y
        self.speed = -10

    def update(self):
        self.rect.y += self.speed
        if self.rect.bottom < 0:
            self.kill()

# Bomb class
class Bomb(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((5, 10))
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.top = y
        self.speed = 5

    def update(self):
        self.rect.y += self.speed
        if self.rect.top > screen_height:
            self.kill()

def show_text(text, x, y, size=36, color=BLACK):
    font = pygame.font.Font(None, size)
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect()
    text_rect.centerx = x
    text_rect.centery = y
    screen.blit(text_surface, text_rect)

def wait_for_space_key():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                return True
        pygame.display.flip()

# Main game logic
def main():
    clock = pygame.time.Clock()
    alien_speed = 1

    while True:
        screen.fill(WHITE)
        show_text("Hit spacebar to start", screen_width // 2, screen_height // 2)
        if not wait_for_space_key():
            break

        global all_sprites
        all_sprites = pygame.sprite.Group()
        aliens = pygame.sprite.Group()
        bullets = pygame.sprite.Group()

        player = Player()
        all_sprites.add(player)

        for i in range(10):
            alien = Alien()
            alien.speed = alien_speed
            all_sprites.add(alien)
            aliens.add(alien)

            #   Add bombs group
        global bombs
        bombs = pygame.sprite.Group()

        running = True
        while running:
            clock.tick(60)
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        bullet = Bullet(player.rect.centerx, player.rect.top)
                        all_sprites.add(bullet)
                        bullets.add(bullet)

            all_sprites.update()

            # Collision detection
            hits = pygame.sprite.groupcollide(aliens, bullets, True, True)
            if not aliens:
                running = False

            # Collision detection
            hits = pygame.sprite.spritecollide(player, aliens, True)
            if hits:
                alien_speed = 1
                running = False
                

            # Check for bomb collisions with the player
            bomb_hits = pygame.sprite.spritecollide(player, bombs, True)
            if bomb_hits:
                alien_speed = 1
                running = False
            
            if not aliens:
                alien_speed += 1
                break

            screen.fill(WHITE)
            show_text(f"Level {alien_speed}", screen_width // 2, 20)
            all_sprites.draw(screen)
            pygame.display.flip()
        
        

    pygame.quit()

if __name__ == "__main__":
    main()