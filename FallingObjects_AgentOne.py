import pygame
import random
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
import os

# Reduce log-level Tensorflow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # FATAL

# Initialisierung von Pygame
pygame.init()

# Bildschirmgröße
screen_width = 100
screen_height = 50
screen = pygame.display.set_mode((screen_width, screen_height))

# Farben
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

# Spielerklasse
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((3, 3))
        self.image.fill(BLACK)
        self.rect = self.image.get_rect(center=(screen_width // 2, screen_height - 3))
        self.speed = 5

    def update(self, action):
        if action == 0 and self.rect.left > 0:  # LEFT
            self.rect.x -= self.speed
        if action == 1 and self.rect.right < screen_width:  # RIGHT
            self.rect.x += self.speed

# Fallendes Objekt
class FallingObject(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((3, 3))
        self.image.fill(RED)
        self.rect = self.image.get_rect(center=(random.randint(0, screen_width), 0))

    def update(self):
        self.rect.y += 5

# Q-Learning Agent
class DQNAgent:
    def __init__(self):
        self.state_size = 2
        self.action_size = 2
        self.memory = []
        self.gamma = 0.95
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.01
        self.learning_rate = 0.001
        self.model = self._build_model()

    def _build_model(self):
        model = Sequential()
        model.add(Dense(24, input_dim=self.state_size, activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(learning_rate=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = (reward + self.gamma * np.amax(self.model.predict(next_state)[0]))
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def save(self, name):
        self.model.save(name)

# Spiel-Setup
player = Player()
falling_objects = pygame.sprite.Group()
agent = DQNAgent()
batch_size = 32

# Spiel-Schleife
running = True
clock = pygame.time.Clock()
while running:
    state = np.array([player.rect.x, player.rect.y]).reshape(1, -1)
    action = agent.act(state)
    player.update(action)

    # Neue fallende Objekte hinzufügen
    if random.randint(1, 20) == 1:
        new_object = FallingObject()
        falling_objects.add(new_object)

    # Update der herabfallenden Objekte
    for obj in falling_objects:
        obj.update()
        if obj.rect.top > screen_height:
            obj.kill()

    # Kollisionserkennung
    if pygame.sprite.spritecollideany(player, falling_objects):
        reward = -10
        done = True
    else:
        reward = 1
        done = False

    next_state = np.array([player.rect.x, player.rect.y]).reshape(1, -1)
    agent.remember(state, action, reward, next_state, done)

    if len(agent.memory) > batch_size:
        agent.replay(batch_size)

    # Bildschirm aktualisieren
    screen.fill(WHITE)
    falling_objects.draw(screen)
    screen.blit(player.image, player.rect)
    pygame.display.flip()

    # FPS
    clock.tick(30)

    if done:
        running = False

# Modell speichern
agent.save('dqn_model.h5')

pygame.quit()